<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/events', \App\Http\Controllers\EventIndexController::class);
Route::post('/events', \App\Http\Controllers\EventStoreController::class);
Route::post('/events/{id}/confirm', \App\Http\Controllers\EventConfirmAssistance::class);
