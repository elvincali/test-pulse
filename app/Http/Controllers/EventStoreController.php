<?php

namespace App\Http\Controllers;

use App\DTOs\EventStoreData;
use App\Services\EventStoreService;
use App\Support\BaseResponse;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class EventStoreController extends Controller
{
    private EventStoreService $service;

    public function __construct(EventStoreService $service)
    {
        $this->service = $service;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $response = new BaseResponse();

        try {
            DB::beginTransaction();
            $data = new EventStoreData(
                date: Carbon::parse($request->get('date')),
                title: $request->get('title'),
                description: $request->get('description'),
                invitations: $request->get('invitations')
            );
            $this->service->execute($data);

            $response->message = 'Creado correctamente';

            DB::commit();
            return new JsonResponse($response);
        } catch (Throwable $e) {
            DB::rollBack();
            $response->errorResponse(($e->getCode() == 0) ? 2 : $e->getCode(), ($e->getCode() == 0) ? "Error desconocido." : $e->getMessage());
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
