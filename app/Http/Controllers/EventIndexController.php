<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Resources\EventCollection;
use App\Support\BaseResponse;
use Illuminate\Http\JsonResponse;

final class EventIndexController extends Controller
{
    public function __invoke(): JsonResponse
    {
        $response = new BaseResponse();

        $events = new EventCollection(Event::all());

        $response->data = $events;

        return new JsonResponse($response);
    }
}
