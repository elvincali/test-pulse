<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Event extends Model
{
    protected $table = 'events';

    protected $casts = [
        'date' => 'date',
    ];

    public function invitations(): HasMany
    {
        return $this->hasMany(EventInvitation::class);
    }
}
