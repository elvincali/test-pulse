<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class EventInvitation extends Model
{
    protected $table = 'event_invitations';

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
