<?php

namespace App\Services;

use App\DTOs\EventStoreData;
use App\Mail\EventCreated;
use App\Models\Event;
use App\Models\EventInvitation;
use Illuminate\Support\Facades\Mail;

final class EventStoreService
{
    public function execute(EventStoreData $data): void
    {
        $event = new Event();
        $event->date = $data->date;
        $event->title = $data->title;
        $event->description = $data->description;
        $event->save();

        foreach ($data->invitations as $invitation) {
            $invitationModel = new EventInvitation();
            $invitationModel->email = $invitation['email'];
            $invitationModel->event()->associate($event);
            $invitationModel->save();

            Mail::to($invitationModel->email)->send(new EventCreated());
        }
    }
}
