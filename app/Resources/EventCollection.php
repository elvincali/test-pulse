<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

final class EventCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->transform(function($row) {
            return [
                'id' => $row->id,
                'date' => $row->date->format('d/m/Y'),
                'title' => $row->title,
            ];
        });
    }
}
