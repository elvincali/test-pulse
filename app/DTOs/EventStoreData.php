<?php

namespace App\DTOs;

use Carbon\Carbon;

final class EventStoreData
{
    public function __construct(
        public Carbon $date,
        public string $title,
        public string $description,
        public array $invitations
    ){}
}
