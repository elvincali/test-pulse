<?php

namespace App\DTOs;

final class EventInvitationData
{
    public function __construct(
        public string $email
    ){}
}
